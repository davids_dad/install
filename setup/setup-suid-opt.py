#!/usr/bin/python3

suid_mount_point="/home/system/opt/opt-setuid"
file="/etc/fstab"
file="/home/richard/+Files/workshops/system/install/setup/test-fstab"
table=f"/files/{file}"

home_options=["relatime","user_xattr","nodev","nosuid", "errors=remount-ro"]

from augeas import Augeas, augeas

def is_int(n):
    try:
        int(n)
    except:
        return False
    else:
        return True

def leaf(path):
    return path.split("/")[-1]

def is_comment(path):
    return leaf(path).startswith("#comment")

def predecessor(path):
    return a.match(path+"/preceding-sibling::*")[0]

def previous_comment(path):
    pre=predecessor(path)
    return a.get(pre) if is_comment(pre) else None

def augeas_print(path, indent_value=0):
    indent="  "*indent_value
    for n in a.match(f"{path}/*"):
        print(f"{indent}{leaf(n)} = ",end="")
        if is_int(leaf(n)):
            print()
            augeas_print(n, indent_value=indent_value+1)
        else:
            print(f"{a.get(n)}")

def _fstab_match(file_name):
    return a.match(f"{table}/*[file='{file_name}']")

def fstab_match(file_name):
    matches= _fstab_match(file_name)
    if len(matches)==1: return matches[0]
    else: return matches

def fstab_ismatch(file_name):
    matches= _fstab_match(file_name)
    if len(matches) > 1: raise NotImplementedError
    return len(matches) == 1

def fstab_add_opts(file_name, options):
    entry=fstab_match(file_name)
    a.remove(f"{entry}/opt")
    for opt in options:
        a.set(f"{entry}/opt[n]", opt)

def fstab_print(file_name):
    augeas_print( fstab_match(file_name))

# The NO_LOAD makes it much faster
# but we need to manualy load what we need
fast_load=augeas.NO_LOAD|augeas.NO_MODL_AUTOLOAD
a=Augeas(flags=fast_load) #|augeas.SAVE_BACKUP)
a.add_transform ("Fstab.lns", incl=file)
a.load()

#setup /home
fstab_add_opts(file_name="/home", options=home_options)

#setup suid
if not fstab_ismatch(suid_mount_point):
    a.set(f"{table}/01/file", suid_mount_point)

suid_entry=fstab_match(suid_mount_point)
a.set(f"{suid_entry}/spec", suid_mount_point)
a.set(f"{suid_entry}/vfstype", "none")
fstab_add_opts(file_name=suid_mount_point, options=["bind", "relatime", "user_xattr", "suid"] )

augeas_print(table)

try:
  a.save()
except:
  pass
a.save()
