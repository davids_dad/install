#!/usr/bin/augtool -bAf

# The -A makes it much faster
# but we need to manualy load what we need

#transform Fstab.lns incl /etc/fstab
transform Fstab.lns incl /home/richard/+Files/workshops/system/install/setup/test-fstab
load

save
errors

# :tricky: many lines — variable not working, hence pasted

defvar conditional_base /files/home/richard/+Files/workshops/system/install/setup/test-fstab[count(*[file="/home/system/opt/suid"])=0]
set $conditional_base/01/file "/home/system/opt/suid"


#print $conditional_base

defvar entry /files/home/richard/+Files/workshops/system/install/setup/test-fstab/*[file="/home/system/opt/suid"]
set $entry/spec "/home/system/opt/suid"
set $entry/vfstype "none"
#set $entry/opt[1] "bind"
#set $entry/opt[2] "relatime"
#set $entry/opt[3] "user_xattr"
#set $entry/opt[4] "suid"
set $entry/passno 0
set $entry/dump 0

#print *

save
errors
